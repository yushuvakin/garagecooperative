﻿using GarageCooperative.Authorization;
using GarageCooperative.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using GarageCooperative.Services;

namespace GarageCooperative.Data
{
    public static class SeedData
    {
        public static async Task Initialize(IServiceProvider serviceProvider, string testUserPw)
        {
            using (var context = new ApplicationDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<ApplicationDbContext>>()))
            {
                
                // For sample purposes we are seeding 2 users both with the same password.
                // The password is set with the following command:
                // dotnet user-secrets set SeedUserPW <pw>
                // The admin user can do anything
                try
                {
                    var adminID = await EnsureUser(serviceProvider, "Qwerty1234_", "admin@garage.com");
                    await EnsureRole(serviceProvider, adminID, Constants.ContactAdministratorsRole);

                    // allowed user can create and edit contacts that they create
                    var uid = await EnsureUser(serviceProvider,"Manager1234_", "buh@garage.com");
                    await EnsureRole(serviceProvider, uid, Constants.GarageCooperativesBuhRole);

                    var bigboss = await EnsureUser(serviceProvider, "Boss1234_", "boss@garage.com");
                    await EnsureRole(serviceProvider, bigboss, Constants.GarageCooperativesBossRole);

                    var secs = await EnsureUser(serviceProvider, "Security1234_", "sec@garage.com");
                    await EnsureRole(serviceProvider, secs, Constants.GarageCooperativesSecRole);

                    var el = await EnsureUser(serviceProvider, "Electric1234_", "el@garage.com");
                    await EnsureRole(serviceProvider, el, Constants.GarageCooperativesElRole);
                
                    SeedDB(context);
                }
                catch (Exception ex)
                {
                    Console.Write("Fuuuuck");
                }
            }
        }    

        private static async Task<string> EnsureUser(IServiceProvider serviceProvider, 
                                                    string testUserPw, string UserName)
        {
            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();
            var user = await userManager.FindByNameAsync(UserName);
            if (user == null)
            {
                user = new ApplicationUser { UserName = UserName, Email = UserName, OwnName =UserName };
                await userManager.CreateAsync(user, testUserPw);
            }

            return user.Id;
        }

        private static async Task<IdentityResult> EnsureRole(IServiceProvider serviceProvider,
                                                                      string uid, string role)
        {
            IdentityResult IR = null;
            var roleManager = serviceProvider.GetService<RoleManager<IdentityRole>>();

            if (!await roleManager.RoleExistsAsync(role))
            {
                IR = await roleManager.CreateAsync(new IdentityRole(role));
            }

            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();

            var user = await userManager.FindByIdAsync(uid);

            IR = await userManager.AddToRoleAsync(user, role);

            return IR;
        }

        public static void SeedDB(ApplicationDbContext context)
        {
               if (!context.BoxTypes.Any())
                {
                    context.BoxTypes.AddRange(
                        new BoxType { Type = 1, Value = 100 },
                        new BoxType { Type = 2, Value = 200 },
                        new BoxType { Type = 3, Value = 300 }
                    );
                    context.SaveChanges();
                }

                if (!context.Boxes.Any())
                {
                    List<BoxType> boxTypes = context.BoxTypes.ToList();

                    context.Boxes.AddRange(
                        new Box { Number = "2/1", BoxTypeId = boxTypes.FirstOrDefault().Id },
                        new Box { Number = "1/2", BoxTypeId = boxTypes.FirstOrDefault().Id },
                        new Box { Number = "1/3", BoxTypeId = boxTypes.Where(x => x.Type == 2).FirstOrDefault().Id },
                        new Box { Number = "1/4", BoxTypeId = boxTypes.Where(x => x.Type == 3).FirstOrDefault().Id }
                    );

                    context.SaveChanges();

                }
            if (!context.BoxTypes.Any())
            {
                context.BoxTypes.AddRange(
                    new BoxType { Type = 1, Value = 100 },
                    new BoxType { Type = 2, Value = 200 },
                    new BoxType { Type = 3, Value = 300 }
                );
                context.SaveChanges();
            }           
        }
    }
}