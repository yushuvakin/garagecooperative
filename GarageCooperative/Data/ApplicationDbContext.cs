﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using GarageCooperative.Models;
using Microsoft.AspNetCore.Identity;

namespace GarageCooperative.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)

        { 
             builder.Entity<Owner>()
            .HasIndex(u => u.Name)
            .IsUnique(true);
            builder.Entity<Car>()
            .HasIndex(u => u.Number)
            .IsUnique(true);
            builder.Entity<OwnerBoxRel>()
            .HasKey(t => new { t.BoxId, t.OwnerId });
            builder.Ignore<IdentityUserToken<string>>();
            builder.Ignore<IdentityUserClaim<string>>();
            builder.Ignore<IdentityUserLogin<string>>();
            builder.Ignore<IdentityRoleClaim<string>>();
            base.OnModelCreating(builder);

            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        public DbSet<GarageCooperative.Models.Owner> Owners { get; set; }

        public DbSet<GarageCooperative.Models.Passport> Passports { get; set; }

        public DbSet<GarageCooperative.Models.Car> Cars { get; set; }

        public DbSet<GarageCooperative.Models.Box> Boxes { get; set; }

        public DbSet<GarageCooperative.Models.ElectricMeterReading> ElectricMeterReadings { get; set; }

        public DbSet<GarageCooperative.Models.CreditDebit> CreditDebit { get; set; }

        public DbSet<GarageCooperative.Models.Debit> Debit { get; set; }

        public DbSet<GarageCooperative.Models.ArrivalJournalRecord> ArrivalJournal { get; set; }

        public DbSet<GarageCooperative.Models.EventJournalRecord> EventJournal { get; set; }

        public DbSet<GarageCooperative.Models.Log> Log { get; set; }

        public DbSet<GarageCooperative.Models.Contract> Contract { get; set; }

        public DbSet<GarageCooperative.Models.OwnerBoxRel> OwnerBoxRel { get; set; }

        public DbSet<GarageCooperative.Models.BoxType> BoxTypes { get; set; }


        public DbSet<GarageCooperative.Models.Balance> balance { get; set; }
    }
}
