﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Services
{
    public interface ISmsSender
    {
       bool SendSms(string number, string message);
    }
}
