﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using MailKit.Net.Smtp;
using System.Threading.Tasks;
using System.Web;
using System.Diagnostics;
using System.Net;

namespace GarageCooperative.Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class SmsSender : ISmsSender
    {

        public bool SendSms(string number, string message)
        {
            string username = "z1516072254883";

            string password = "544542";
            string msgsender = "PASK Zagorie-5";
            string destinationaddr = number;
            string url;
            // Build the URL request for sending SMS.
            url = "http://api.iqsms.ru/messages/v2/send/?"
                + "login=" + username
                + "&password=" + password
                + "&phone=" + destinationaddr
                + "&text=" + message;
            // + "&refno=1";
            WebClient client = new WebClient();
            string reply = client.DownloadString(url);
            return reply.Contains("accepted");
        }
    }
}
