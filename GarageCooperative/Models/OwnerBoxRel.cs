﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class OwnerBoxRel
    {
        [Required]
        public int BoxId { get; set; }
        [Required]
        [ForeignKey("BoxId")]
        public Box Box { get; set; }
        [Required]
        public int OwnerId { get; set; }
        [Required]
        [ForeignKey("OwnerId")]
        public  Owner Owner { get; set; }
    }
}
