﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.BoxesViewModel
{
    public class BoxesViewModel
    {
        public string Status { get; set; }
        public List<Box> boxes { get; set; }
        public BoxesViewModel()
        {
            boxes = new List<Box>();
        }
    }
}
