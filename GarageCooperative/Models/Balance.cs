﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class Balance
    { 
        public int id { get; set; }
        public double balance { get; set; }
    }
}
