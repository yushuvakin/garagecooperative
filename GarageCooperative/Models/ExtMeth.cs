﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{

    public static class UserManagerExtensions
    {
        public static ApplicationUser FindByNameAsync(this UserManager<ApplicationUser> um, ClaimsPrincipal name)
        {
            return um?.Users?.SingleOrDefault(x => x.UserName == um.GetUserName(name));
        }

    }
}