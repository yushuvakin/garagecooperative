﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.CarsViewModels
{
    public class EnterViewModel
    {
        public string CarNumber { get; set; }
        public string CarColor { get; set; }
        public string CarModel { get; set; }
    }
}
