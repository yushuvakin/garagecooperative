﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.CarsViewModels
{
    public class CheckViewModel
    {
        public string Status { get; set; }
        public string CarNumber { get; set; }
        public string CarColor { get; set; }
        public string CarModel { get; set; }
        public List<Owner> selectedOwners { get; set; }
        public CheckViewModel()
        {
            selectedOwners = new List<Owner>();
        }
    }
}
