﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.ArrivalJournalModels
{
    public class EventJournalRecordView
    {
            public string Status { get; set; }
            public EventJournalRecord record { get; set; }
    }
}
