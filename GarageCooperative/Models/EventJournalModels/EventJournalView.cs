﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.ArrivalJournalModels
{
    public class EventJournalView
    {
            public string Status { get; set; }
            public List<EventJournalRecord> journal { get; set; }
    }
}
