﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.OwnersViewModels
{
    public class DeleteOwnerView
    {
            public string OwnerName { get; set; }
            public int Id { get; set; }
    }
}
