﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.OwnersViewModels
{
    public class OwnerModelView
    {
       public string Status { get; set; }
       public Owner Owner { get; set; }
    }
}
