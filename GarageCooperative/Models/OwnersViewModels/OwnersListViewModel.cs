﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.OwnersViewModels
{
    public class OwnersListViewModel
    {
        public string Status { get; set; }
        public List<Owner> Owners { get; set; }

        public OwnersListViewModel()
        {
            Owners = new List<Owner>();
        }
    }
}
