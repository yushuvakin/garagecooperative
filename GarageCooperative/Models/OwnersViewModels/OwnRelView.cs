﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.OwnersViewModels
{
    public class OwnerRelView
    {
         [Required]
       public string BoxNumber { get; set; }
       public int OwnerId { get; set; }
       public string Status { get; set; }
    }
}
