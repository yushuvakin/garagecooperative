﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class ArrivalJournalRecord
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Поле Номер машины не заполнено")]
        public string CarNumber { get; set; }
        public int? BoxId { get; set; }
        public Box Box { get; set; }
        public DateTime Data { get; set; }
        [Required(ErrorMessage = "Поле ФИО не заполнено")]
        public string Name  { get; set; }
        public string Purpose { get; set; }

    }
}
