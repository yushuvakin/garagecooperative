﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.ArrivalJournalModels
{
    public class ArrivalJournalRecordView
    {
            public string Status { get; set; }
            public ArrivalJournalRecord record { get; set; }
            public string BoxNumber { get; set; }
    }
}
