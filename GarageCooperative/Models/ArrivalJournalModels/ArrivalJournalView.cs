﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.ArrivalJournalModels
{
    public class ArrivalJournalView
    {
            public string Status { get; set; }
            public List<ArrivalJournalRecord> journal { get; set; }
    }
}
