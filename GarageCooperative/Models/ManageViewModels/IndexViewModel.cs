﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.ManageViewModels
{
    public class IndexViewModel
    {
        public string Username { get; set; }
        public string Ownname { get; set; }
        public bool IsEmailConfirmed { get; set; }

        [Required(ErrorMessage = "Поле Email не заполнено")]
        [EmailAddress(ErrorMessage = "Неправильный формат Email")]
        public string Email { get; set; }
        /*
        [Phone]
        [Display(Name = "Телефон")]
        public string PhoneNumber { get; set; }
        */
        public string StatusMessage { get; set; }
    }
}
