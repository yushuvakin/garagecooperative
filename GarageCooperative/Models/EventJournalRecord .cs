﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class EventJournalRecord
    {
        public int Id { get; set; }
        public DateTime Data { get; set; }
        [Required(ErrorMessage = "Описание на заполнено")]
        public string Decription { get; set; }

    }
}
