﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class CreditDebit
    {
        public int Id { get; set; }
        [Required]
        public string Target { get; set; }
        [Required]
        public bool Credit { get; set; }
        public double Value { get; set; }

        public bool BuhAccepted { get; set; }
        public DateTime Data { get; set; }
        public DateTime ConfirmationData { get; set; }
    }
}
