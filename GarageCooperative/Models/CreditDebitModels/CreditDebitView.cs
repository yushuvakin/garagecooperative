﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.CreditDebitModels
{
    public class CreditDebitView
    {
        public string Status { get; set; }
        public List<CreditDebit> list { get; set; }
        public double Balance { get; set; }

    }
}
