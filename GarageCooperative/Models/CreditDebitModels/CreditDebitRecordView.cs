﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.CreditDebitModels
{
    public class CreditDebitRecordView
    {
        public string Status { get; set; }
        public CreditDebit record { get; set; }
        public double Balance { get; set; }

        public CreditDebitRecordView()
        {
            record = new CreditDebit();
        }

    }
}
