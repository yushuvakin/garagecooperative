﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace GarageCooperative.Models
{
    public class Log
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int GarageNumber { get; set; }
        public int ContractNumber { get; set; }
        public string BookNumber { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ReleaseDate { get; set; }
    }
}
