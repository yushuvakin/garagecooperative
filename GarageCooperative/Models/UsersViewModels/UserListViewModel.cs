﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.UsersViewModels
{
    public class UserListViewModel
    {
        public string Status { get; set; }
        public List<ApplicationUser> Userlist { get; set; }
        public UserListViewModel()
        {
            Userlist = new List<ApplicationUser>();
        }
    }
}
