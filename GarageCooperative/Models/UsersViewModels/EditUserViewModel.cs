﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.UsersViewModels
{
    public class EditUserViewModel
    {
        public List<IdentityRole> AllRoles { get; set; }
        public IList<string> UserRoles { get; set; }
        public EditUserViewModel()
        {
            AllRoles = new List<IdentityRole>();
            UserRoles = new List<string>();
        }

       /* public void fillRole()
        {
            AllRoles = new List<IdentityRole>();
            UserRoles = new List<string>();
        }*/

        [Required(ErrorMessage = "Поле Email не заполнено")]
        [EmailAddress(ErrorMessage = "Неправильный формат Email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле Имя не заполнено")]
        [Display(Name = "Имя")]
        public string OwnName { get; set; }
        public string Id { get; set; }
        public bool PasswordChange { get; set; }
        public string RoleId { get; set; }
}
}
