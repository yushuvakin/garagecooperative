﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.UsersViewModels
{
    public class CreateUserViewModel
    {

        public List<IdentityRole> AllRoles { get; set; }
        public CreateUserViewModel()
        {
            AllRoles = new List<IdentityRole>();
        }
        [Required(ErrorMessage = "Поле Логин не заполнено")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Поле Email не заполнено")]
        [EmailAddress(ErrorMessage = "Неправильный формат Email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Поле Пароль не заполнено")]
        [StringLength(100, ErrorMessage = "Длина пароля минимум {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Поле Подтверждение пароля не заполнено")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }
   
        [Required(ErrorMessage = "Поле Имя не заполнено")]
        [Display(Name = "Имя")]
        public string OwnName { get; set; }
        [Required(ErrorMessage = "Please select a role")]
        public string RoleId { get; set; }
    }
}
