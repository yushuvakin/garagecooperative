﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.UsersViewModels
{
    public class DeleteUserViewModel
    {

        public string UserName { get; set; }
        public string  Id { get; set; }

    }
}
