﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class BoxType
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int Type { get; set; }
        [Required]
        public float Value { get; set; }
        public List<Box> Boxes { get; set; }
    }
}
