﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.ElectricMeterReadingViewModels
{
    public class CreateElectricMeterReadingViewModel
    {
        [Required(ErrorMessage = "Бокс не выбран")]
        public int? BoxId { get; set; }

        [Required(ErrorMessage = "Значение не заполнено")]
        [Range(0, System.Int32.MaxValue, ErrorMessage = "Значение выходит за пределы диапазона")]
        public int? Value { get; set; }
    }
}
