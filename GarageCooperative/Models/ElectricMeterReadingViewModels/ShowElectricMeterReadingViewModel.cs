﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models.ElectricMeterReadingViewModels
{
    public class ShowElectricMeterReadingViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Номер бокса")]
        public string BoxNumber { get; set; }

        [Display(Name = "Значение")]
        public int Value { get; set; }

        [Display(Name = "Дата")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:hh:mm:ss dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public ShowElectricMeterReadingViewModel(ElectricMeterReading meter)
        {
            Id = meter.Id;
            Value = meter.Value;
            Date = meter.Date;
            BoxNumber = meter.Box.Number;
        }
    }
}
