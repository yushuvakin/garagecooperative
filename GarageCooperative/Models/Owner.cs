﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class Owner
    {
        public Owner()
        {
            Cars = new List<Car>();
            Passport = new List<Passport>();
            OwnerBoxRels = new List<OwnerBoxRel>();
        }
        [Key]
        public int Id { get; set; }
        [Required(ErrorMessage = "Поле ФИО не заполнено")]
        [MaxLength(150, ErrorMessage = "Максимальная длина поля ФИО 150 символов")]
        public string Name { get; set; }
        public List<Passport> Passport { get; set; }
        public Contract Contract { get; set; }
        public List<Car> Cars { get; set; }
        public string Address { get; set; }
        public string TelephoneNumber { get; set; }
        public string TelephoneNumberAdd { get; set; }
        public string Photo { get; set; }
        public float Balance { get; set; }
        public List<OwnerBoxRel> OwnerBoxRels { get; set; }

    }

    public enum SortStateOwner
    {
        NameAsc,    // по имени по возрастанию
        NameDesc,   // по имени по убыванию
        AgeAsc, // по возрасту по возрастанию
        AgeDesc,    // по возрасту по убыванию
        CompanyAsc, // по компании по возрастанию
        CompanyDesc // по компании по убыванию
    }
}
