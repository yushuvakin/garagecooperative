﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class Box
    {
        public int Id { get; set; }
        [Required (ErrorMessage ="Поле Номер бокса не заполнено")]
        public string Number { get; set; }
        [Display(Name = "Повышенное энергопотребление")]
        public bool MuchEnergyConsumption { get; set; }
        public int BoxTypeId { get; set; }
        [ForeignKey("BoxTypeId")]
        public BoxType BoxType { get; set; }
        public List<OwnerBoxRel> OwnerBoxRels { get; set; }

        public List<ArrivalJournalRecord> records { get; set; }
        public Box()
        {
            OwnerBoxRels = new List<OwnerBoxRel>();
           records = new List<ArrivalJournalRecord>();
        }
    }
}
