﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class Debit
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int GarageNumber { get; set; }
        public int ContractNumber { get; set; }
        public string BookNumber { get; set; }
        public double Value { get; set; }
        public string Date { get; set; }
    }
}
