﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class Passport
    {
        public int Id { get; set; }
        public int Seria { get; set; }
        [Required]
        public int Number { get; set; }
        public string PlaceOfIssue { get; set; }
        public string PlaceOfBirth { get; set; }
        // [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy
        [DataType(DataType.Date)]
        public DateTime  DateOfIssue { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        public string Photo1 { get; set; }
        public string Photo2 { get; set; }
        public int OwnerId { get; set; }
        [ForeignKey("OwnerId")]
        public Owner Owner { get; set; }
    }
}
