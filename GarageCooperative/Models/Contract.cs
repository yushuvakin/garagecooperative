﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace GarageCooperative.Models
{
    public class Contract
    {
        [Required]
        public int Id { get; set; }
        public string Number { get; set; }
        public string Photo1 { get; set; }
        public string Photo2 { get; set; }
        public int OwnerId { get; set; }
        [ForeignKey("OwnerId")]
        public Owner Owner { get; set; }
    }
}
