﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GarageCooperative.Data;
using GarageCooperative.Models;
using Microsoft.AspNetCore.Authorization;

namespace GarageCooperative.Controllers
{
    [Authorize(Roles = "Buh")]
    public class DebitsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public DebitsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Debits
        public async Task<IActionResult> Index()
        {
            return View(await _context.Debit.ToListAsync());
        }

        // GET: Debits/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var debit = await _context.Debit
                .SingleOrDefaultAsync(m => m.Id == id);
            if (debit == null)
            {
                return NotFound();
            }

            return View(debit);
        }

        // GET: Debits/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Debits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,GarageNumber,ContractNumber,BookNumber,Value,Date")] Debit debit)
        {
            if (ModelState.IsValid)
            {
                _context.Add(debit);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(debit);
        }

        // GET: Debits/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var debit = await _context.Debit.SingleOrDefaultAsync(m => m.Id == id);
            if (debit == null)
            {
                return NotFound();
            }
            return View(debit);
        }

        // POST: Debits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,GarageNumber,ContractNumber,BookNumber,Value,Date")] Debit debit)
        {
            if (id != debit.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(debit);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DebitExists(debit.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(debit);
        }

        // GET: Debits/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var debit = await _context.Debit
                .SingleOrDefaultAsync(m => m.Id == id);
            if (debit == null)
            {
                return NotFound();
            }

            return View(debit);
        }

        // POST: Debits/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var debit = await _context.Debit.SingleOrDefaultAsync(m => m.Id == id);
            _context.Debit.Remove(debit);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DebitExists(int id)
        {
            return _context.Debit.Any(e => e.Id == id);
        }
    }
}
