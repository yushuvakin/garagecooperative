﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GarageCooperative.Data;
using GarageCooperative.Models;
using GarageCooperative.Models.CarsViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace GarageCooperative.Controllers
{
    [Authorize(Roles = "Администратор,Охранник")]
    public class CarsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CarsController(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public IActionResult Index()
        {
            EnterViewModel model = new EnterViewModel();

            return View(model);
        }

        [HttpPost]
        public IActionResult Check(EnterViewModel model)
        {
            List<Owner> selectedOwners = new List<Owner>();
            string Status;
            if (model.CarNumber != null || (model.CarModel != null && model.CarColor != null))
            {
                IQueryable<Owner> Owners = _context.Owners.Include(x => x.OwnerBoxRels).ThenInclude(y => y.Box).Include(x => x.Cars);

                foreach (Owner owner in Owners.ToList())
                {
                    List<Car> findedCars = owner.Cars.Where(y => (y.Model.Equals(model.CarModel, StringComparison.OrdinalIgnoreCase) && y.Color.Equals(model.CarColor, StringComparison.OrdinalIgnoreCase) ||
                    y.Number.Equals(model.CarNumber, StringComparison.OrdinalIgnoreCase))).ToList();
                    if (findedCars.Any())
                    {
                        selectedOwners.Add(owner);
                    }
                }
                if (selectedOwners.Any()) Status = null;
                else Status = "Не найдено совпадений";


            }
            else
                Status = "Введены неверные значения для поиска. Необходимо ввести номер машины или модель вместе с цветом машины";
            CheckViewModel model1 = new CheckViewModel()
            {
                Status = Status,
                CarColor = model.CarColor,
                CarModel = model.CarModel,
                CarNumber = model.CarNumber,
                selectedOwners = selectedOwners,
            };

           return View(model1); 

        }


        // GET: Owners/Create
        public IActionResult Create(int id)
        {
            Car car = new Car();
            car.OwnerId = id;
            return View(car);
        }

        // POST: Owners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Car car)
        {
            if (ModelState.IsValid)
            {
                //var owner = _context.Owners.SingleOrDefault(w => w.Id == passport.OwnerId);
                //owner.Passport.Add(passport);
                // owner.Contract.Number = contractNumber;
                car.Id = 0;
                _context.Add(car);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Owners", new { id = car.OwnerId, status = String.Format("Машина добавлена успешно") });
            }
            return View(car);
        }


        public async Task<ActionResult> Delete(int id)
        {
           Car car= _context.Cars.SingleOrDefault(w => w.Id == id);
            return View(car);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteConfirm(int id)
        {
            Car car = _context.Cars.SingleOrDefault(w => w.Id == id);
            if (car != null)
            {

                _context.Remove(car);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Owners", new { id = car.OwnerId, status = String.Format("Машина успешно удалена") });
            }
            return RedirectToAction("Details", "Owners", new { id = car.OwnerId, status = "Произошла ошибка" });

        }
        public ActionResult Cancel(int id)
        {
            return RedirectToAction("Details", "Owners", new { id = id });
        }

        // GET: Owners/Details/5
        public async Task<IActionResult> Edit(int? id, string status = null)
        {
            if (id == null)
            {
                return NotFound();
            }

            var car = _context.Cars.SingleOrDefault(m => m.Id == id);
            if (car== null)
            {
                return NotFound();
            }

            return View(car);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Car car)
        {
            if (ModelState.IsValid)
            {
                _context.Update(car);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Owners", new { id = car.OwnerId, status = "Данные машины успешно изменены" });
            }
            return View(car);
        }


    }
}