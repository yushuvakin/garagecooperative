﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using GarageCooperative.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using GarageCooperative.Services;
using GarageCooperative.Data;

namespace GarageCooperative.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly ApplicationDbContext _context;

        public HomeController(ApplicationDbContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        public IActionResult Sms()
        {

            return View();

        }

        [HttpPost]
        public IActionResult Sms(string Message)
        {
            SmsSender sms = new SmsSender();
            //result = false;
            //sms.SendSms("+79037143055", Message);// номер рысиной, кому надо) мифи 29 кафедра
            sms.SendSms("+79150632501", Message);
            /*   if (Message != null)
                {
                    bool isNumber=false;
                    foreach (Owner owner in _context.Owners.ToList())
                    {

                        if (owner.TelephoneNumber != null && owner.TelephoneNumber.Equals("+79150632501")) isNumber = true;
                        if (owner.TelephoneNumberAdd != null && owner.TelephoneNumberAdd.Equals("+79150632501")) isNumber = true;

                    }
                    bool result = false;
                    if (isNumber)
                    {
                        SmsSender sms = new SmsSender();
                        //result = false;
                        sms.SendSms("+79037143055", Message);
                    }

                    View("Sms", "Сообщения успешно отосланы");
                   // else return View("Sms", "Ошибка при отправке");
                }
                */
            return View("Sms", "Сообщение пустое. Пустые сообщения не отправляются");
                
        }
    }
}
