﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GarageCooperative.Data;
using GarageCooperative.Models;
using Microsoft.AspNetCore.Authorization;

namespace GarageCooperative.Controllers
{
    [Authorize(Roles = "Администратор,Председатель")]
    public class BoxesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public BoxesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Boxes
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Boxes.Include(b => b.BoxType);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: Boxes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var box = await _context.Boxes
                .Include(b => b.BoxType)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (box == null)
            {
                return NotFound();
            }

            return View(box);
        }

           // GET: Boxes/Create
        public IActionResult Create()
        {
            ViewData["BoxTypeId"] = new SelectList(_context.BoxTypes, "Id", "Id");
            return View();
        }

        // POST: Boxes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Number,MuchEnergyConsumption,BoxTypeId")] Box box)
        {
            if (ModelState.IsValid)
            {
                _context.Add(box);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["BoxTypeId"] = new SelectList(_context.BoxTypes, "Id", "Id", box.BoxTypeId);
            return View(box);
        }
        // GET: Boxes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var box = await _context.Boxes.SingleOrDefaultAsync(m => m.Id == id);
            if (box == null)
            {
                return NotFound();
            }
            ViewData["BoxTypeId"] = new SelectList(_context.BoxTypes, "Id", "Id", box.BoxTypeId);
            return View(box);
        }

        // POST: Boxes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Number,MuchEnergyConsumption,BoxTypeId")] Box box)
        {
            if (id != box.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(box);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BoxExists(box.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BoxTypeId"] = new SelectList(_context.BoxTypes, "Id", "Id", box.BoxTypeId);
            return View(box);
        }

        // GET: Boxes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var box = await _context.Boxes
                .Include(b => b.BoxType)
                .SingleOrDefaultAsync(m => m.Id == id);
            if (box == null)
            {
                return NotFound();
            }

            return View(box);
        }

        // POST: Boxes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var box = await _context.Boxes.SingleOrDefaultAsync(m => m.Id == id);
            _context.Boxes.Remove(box);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BoxExists(int id)
        {
            return _context.Boxes.Any(e => e.Id == id);
        }
    }
}
