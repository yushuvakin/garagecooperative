﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GarageCooperative.Data;
using GarageCooperative.Models;
using GarageCooperative.Models.ElectricMeterReadingViewModels;
using Microsoft.AspNetCore.Authorization;

namespace GarageCooperative.Controllers
{
    [Authorize(Roles = "Администратор,Электрик")]
    public class ElectricMeterReadingsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ElectricMeterReadingsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: ElectricMeterReadings
        public async Task<IActionResult> Index()
        {
            List<ShowElectricMeterReadingViewModel> meters = await _context.ElectricMeterReadings.Include("Box").Select(
                x => new ShowElectricMeterReadingViewModel(x)).ToListAsync();
            return View(meters);
        }

        // GET: ElectricMeterReadings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var electricMeterReading = await _context.ElectricMeterReadings
                .SingleOrDefaultAsync(m => m.Id == id);
            if (electricMeterReading == null)
            {
                return NotFound();
            }

            return View(electricMeterReading);
        }

        // GET: ElectricMeterReadings/Create
        public IActionResult Create()
        {
            ViewBag.BoxList = _context.Boxes.ToList();
            return View();
        }

        // POST: ElectricMeterReadings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreateElectricMeterReadingViewModel meterViewModel)
        {
            if (ModelState.IsValid)
            {
                var meter = new ElectricMeterReading
                {
                    Box = _context.Boxes.Where(x => x.Id == meterViewModel.BoxId).FirstOrDefault(),
                    Value = meterViewModel.Value.GetValueOrDefault(),
                    Date = DateTime.Now
                };

                _context.Add(meter);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            ViewBag.BoxList = _context.Boxes.ToList();
            return View(meterViewModel);
        }

        // GET: ElectricMeterReadings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var electricMeterReading = await _context.ElectricMeterReadings.SingleOrDefaultAsync(m => m.Id == id);
            if (electricMeterReading == null)
            {
                return NotFound();
            }
            return View(electricMeterReading);
        }

        // POST: ElectricMeterReadings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Value,Date")] ElectricMeterReading electricMeterReading)
        {
            if (id != electricMeterReading.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(electricMeterReading);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ElectricMeterReadingExists(electricMeterReading.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(electricMeterReading);
        }

        // GET: ElectricMeterReadings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var meters = await _context.ElectricMeterReadings.Include("Box").Select(
                x => new ShowElectricMeterReadingViewModel(x)).ToListAsync();

            var electricMeterReadingViewModel = meters.AsQueryable().SingleOrDefault(x => x.Id == id);
            if (electricMeterReadingViewModel == null)
            {
                return NotFound();
            }

            return View(electricMeterReadingViewModel);
        }

        // POST: ElectricMeterReadings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var electricMeterReading = await _context.ElectricMeterReadings.SingleOrDefaultAsync(m => m.Id == id);
            _context.ElectricMeterReadings.Remove(electricMeterReading);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ElectricMeterReadingExists(int id)
        {
            return _context.ElectricMeterReadings.Any(e => e.Id == id);
        }
    }
}
