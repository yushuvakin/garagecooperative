﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GarageCooperative.Data;
using GarageCooperative.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using GarageCooperative.Models.OwnersViewModels;
using GarageCooperative.Services;

namespace GarageCooperative.Controllers
{
    [Authorize(Roles = "Администратор,Председатель")]
    public class OwnersController : Controller
    {
        private readonly ApplicationDbContext _context;
        IHostingEnvironment _appEnvironment;

        public OwnersController(ApplicationDbContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
            /*      var Owners = new Owner[]
                    {
                    new Owner{Name="Королев Степан Степаныч",  Cars = new List<Car>
                    {new Car{Model="nissan", Number="к444хк99", Color="красный" } ,
                      new Car{Model="dodge", Number="к656кк34", Color="зеленый" } ,
                    } ,
                    Passport = new List<Passport>
                    {new Passport{ Seria=1111, Number=222222, DateOfIssue=new DateTime (2015,11,10), PlaceOfIssue = "Москва" }
                    },
                    Contract = new Contract{Number="123"},
                    Address="Проезд чайников",
                    TelephoneNumber="+744433322211",
                    TelephoneNumberAdd="+788883333444",
                    Balance=-1000.5f
                    },
                    new Owner{Name="Игрев Вадим Вадимыч", Cars = new List<Car>
                    {new Car{Model="nissan", Number="к344хк99", Color="желтый" } },
                    Balance=-100.5f,

                    },

                    new Owner{Name="Хренов Владимир Владимирович" ,
                    Passport = new List<Passport>
                    {new Passport{ Seria=1111, Number=222222, DateOfIssue=new DateTime (2015,11,10), PlaceOfIssue = "Москва" },
                    new Passport{ Seria=1134, Number=222342, DateOfIssue=new DateTime (2015,11,10), PlaceOfIssue = "Москва" },
                    },
                    Contract = new Contract{Number="1234"},
                    TelephoneNumber="+744433322211",
                    },
                    new Owner{Name="Хренова Анна Анатольевна" ,
                    Passport = new List<Passport>
                    {new Passport{ Seria=1111, Number=222222, DateOfIssue=new DateTime (2015,11,10), PlaceOfIssue = "Москва" },
                    new Passport{ Seria=11354, Number=222342, DateOfIssue=new DateTime (2015,11,10), PlaceOfIssue = "Москва" },
                    },
                    Contract = new Contract{Number="12234"},
                    Balance=-200.5f,
                    },
                   };

                    var BoxTypes = new BoxType[]
                    {
                    new BoxType{Type = 1, Value=100 },
                    new BoxType{Type = 2, Value=200 },
                    new BoxType{Type = 3, Value=300 },
                    };


                    foreach (Owner s in Owners)
                    {
                        _context.Owners.Add(s);
                    }

                    foreach (BoxType s in BoxTypes)
                    {
                        _context.BoxTypes.Add(s);
                    }

                    _context.SaveChanges();
                    try
                    { _context.SaveChanges(); }
                    catch (Exception ex) { }

                    List<BoxType> boxTypes = _context.BoxTypes.ToList();
                    var Boxes = new Box[]
                   {
                    new Box{Number="2/1", BoxTypeId=boxTypes.FirstOrDefault().Id },
                    new Box{Number="1/2", BoxTypeId=boxTypes.FirstOrDefault().Id  },
                    new Box{Number="1/3", BoxTypeId=boxTypes.Where(x=>x.Type==2).FirstOrDefault().Id },
                    new Box{Number="1/4", BoxTypeId=boxTypes.Where(x=>x.Type==3).FirstOrDefault().Id  },
                    };

                    foreach (Box s in Boxes)
                    {
                        _context.Boxes.Add(s);
                    }

                    try
                    { _context.SaveChanges(); }
                    catch (Exception ex) { }
                    /*
                List<Owner> ownerback = _context.Owners.ToList();
                List<Box> boxesback = _context.Boxes.ToList();
                OwnerBoxRel ff = new OwnerBoxRel();
                //{
                 //   OwnerId = ownerback.Where(x => x.Name == "Королев Степан Степаныч").FirstOrDefault().Id,
                  //  BoxId = boxesback.Where(x => x.Number.Equals("2/1")).FirstOrDefault().Id
                //};
                //ownerback.Where(x => x.Name == "Королев Степан Степаныч").FirstOrDefault().OwnerBoxRels.Add(ff);
                //boxesback.Where(x => x.Number.Equals("2/1")).FirstOrDefault().OwnerBoxRels.Add(ff);
              //  _context.Owners.AsNoTracking(). ToList().Where(x => x.Name == "Королев Степан Степаныч").FirstOrDefault() .OwnerBoxRels.Add(ff);
               // _context.Boxes.AsNoTracking().ToList().Where(x => x.Number.Equals("2/1")).FirstOrDefault().OwnerBoxRels.Add(ff);
                //_context.Update(_context.Owners.ToList().Where(x => x.Name == "Королев Степан Степаныч").FirstOrDefault());
                //_context.Update(_context.Boxes.ToList().Where(x => x.Number.Equals("2/1")).FirstOrDefault());
                var OwnBoxRels = new OwnerBoxRel[]
                {
                    new OwnerBoxRel{OwnerId=ownerback.Where(x=>x.Name=="Королев Степан Степаныч").FirstOrDefault().Id,
                    BoxId = boxesback.Where(x=>x.Number.Equals("2/1")).FirstOrDefault().Id },
                    new OwnerBoxRel{OwnerId=ownerback.Where(x=>x.Name=="Королев Степан Степаныч").FirstOrDefault().Id,
                    BoxId = boxesback.Where(x=>x.Number.Equals("1/2")).FirstOrDefault().Id },
                    new OwnerBoxRel{OwnerId=ownerback.Where(x=>x.Name=="Игрев Вадим Вадимыч").FirstOrDefault().Id,
                    BoxId = boxesback.Where(x=>x.Number.Equals("1/4")).FirstOrDefault().Id },
                    new OwnerBoxRel{OwnerId=ownerback.Where(x=>x.Name=="Хренов Владимир Владимирович").FirstOrDefault().Id,
                    BoxId = boxesback.Where(x=>x.Number.Equals("1/3")).FirstOrDefault().Id },
                    new OwnerBoxRel{OwnerId=ownerback.Where(x=>x.Name=="Хренова Анна Анатольевна").FirstOrDefault().Id,
                    BoxId = boxesback.Where(x=>x.Number.Equals("1/3")).FirstOrDefault().Id }
                };
                foreach (OwnerBoxRel s in OwnBoxRels)
                {


                _context.OwnerBoxRel.Add(s);
                }
               // _context.SaveChanges();
                try
                { _context.SaveChanges(); }
                catch (Exception ex) { }
               */
        }

        // GET: Owners
        public async Task<IActionResult> Index(string status = null)
        {
            IQueryable<Owner> users = _context.Owners.Include(x => x.OwnerBoxRels).ThenInclude(y => y.Box).Include(x => x.Cars);
            OwnersListViewModel owners = new OwnersListViewModel
            {
                Status = status,
                Owners = users.ToList().OrderBy(x => x.OwnerBoxRels != null && x.OwnerBoxRels.Count > 0
                ? x.OwnerBoxRels[x.OwnerBoxRels.Count - 1].Box.Number
                : string.Empty).ToList()
            };

            return View(owners);
        }
        public async Task<IActionResult> FindDebtors()
        {
            IQueryable<Owner> users = _context.Owners.Include(x => x.OwnerBoxRels).ThenInclude(y => y.Box);
            return View(users.Where(y => y.Balance < 0).ToList().OrderBy(x => x.Balance));
        }
        // GET: Owners/Details/5
        public async Task<IActionResult> Details(int id, string status = null)
        {
            if (id == null)
            {
                return NotFound();
            }


            var owner = _context.Owners.Include(x => x.Passport).Include(x => x.Cars).Include(x => x.Contract)
            .Include(x => x.OwnerBoxRels).ThenInclude(y => y.Box).ThenInclude(z => z.BoxType)
            .SingleOrDefaultAsync(m => m.Id == id).Result;
            if (owner == null)
            {
                return NotFound();
            }
            //
            var contract = _context.Contract.Where(e => e.OwnerId == id);

            if (contract == null)
            {
                owner.Contract = new Contract { OwnerId = id };
                // Contract x = new Contract { OwnerId = id };
                _context.Update(owner);
                _context.SaveChanges();
            }
            OwnerModelView OwnerModel = new OwnerModelView
            {
                Status = status,
                Owner = owner
            };
            return View(OwnerModel);
        }

        [HttpPost]
        public async Task<IActionResult> AddPhoto(int? id, IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                var Owner = _context.Owners.SingleOrDefault(m => m.Id == id);
                // путь к папке Files
                string path = "/images/Owner" + id + ".jpg";
                if (path == Owner.Photo) path = "/images/Owner" + id + "_1.jpg";
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }

                Owner.Photo = path;
                _context.Update(Owner);
                _context.SaveChanges();
                return RedirectToAction("Details", new { id = id, status = "Фото успешно загружено" });
            }

            return RedirectToAction("Details", new { id = id });
        }
        // GET: Owners/Create
        public IActionResult Create()
        {
            Owner owner = new Owner();

            return View(owner);
        }

        // POST: Owners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Owner owner)
        {
            if (ModelState.IsValid)
            {
                // owner.Contract.Number = contractNumber;
                _context.Add(owner);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { status = String.Format("Член кооператива {0} был добавлен успешно", owner.Name) });
            }
            return View(owner);
        }

        // GET: Owners/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var owner = await _context.Owners.SingleOrDefaultAsync(m => m.Id == id);
            if (owner == null)
            {
                return NotFound();
            }
            return View(owner);
        }

        // POST: Owners/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(OwnerModelView model)
        {

            if (ModelState.IsValid)
            {
                try


                {
                    var owner = model.Owner;

                    //  _context.Update(_context.Contract.SingleOrDefaultAsync(m => m.OwnerId == model.Owner.Id).Result.Number);
                    _context.Update(model.Owner);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OwnerExists(model.Owner.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("Details", new { id = model.Owner.Id, status = "Данные успешно изменены" });
            }
            return RedirectToAction("Details");
        }

        /*        // GET: Owners/Delete/5
                public async Task<IActionResult> Delete(int? id)
                {
                    if (id == null)
                    {
                        return NotFound();
                    }

                    var owner = await _context.Owners
                        .SingleOrDefaultAsync(m => m.Id == id);
                    if (owner == null)
                    {
                        return NotFound();
                    }

                    return View(owner);
                }

                // POST: Owners/Delete/5
                [HttpPost, ActionName("Delete")]
                [ValidateAntiForgeryToken]
                public async Task<IActionResult> DeleteConfirmed(int id)
                {
                    var owner = await _context.Owners.SingleOrDefaultAsync(m => m.Id == id);
                    _context.Owners.Remove(owner);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                */
        public async Task<ActionResult> Delete(int id)
        {
            var owner = _context.Owners.SingleOrDefault(m => m.Id == id);


            DeleteOwnerView model = new DeleteOwnerView { Id = id, OwnerName = owner.Name };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteOwnerView model)
        {
            var contract = _context.Contract.SingleOrDefault(x => x.OwnerId == model.Id);
            var ownBoxes = _context.OwnerBoxRel.Where(x => x.OwnerId == model.Id);
            var cars = _context.Cars.Where(x => x.OwnerId == model.Id);
            var owner = _context.Owners.SingleOrDefault(m => m.Id == model.Id);
            if (owner != null)
            {
                /*  _context.Remove(contract);
                  foreach (OwnerBoxRel ownBox in ownBoxes)
                  _context.Remove(ownBox);
                  foreach (Car car in cars)
                      _context.Remove(car);*/
                _context.Remove(owner);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { status = System.String.Format("Член кооператива {0} был успешно удален", owner.Name) });
            }
            return RedirectToAction("Index", new { status = "Произошла ошибка" });

        }
        public ActionResult Cancel(DeleteOwnerView model) => RedirectToAction("Index");


        private bool OwnerExists(int id)
        {
            return _context.Owners.Any(e => e.Id == id);
        }

        // GET: Owners/Details/5
        public async Task<IActionResult> PhotoContract(int? id, string status = null)
        {
            if (id == null)
            {
                return NotFound();
            }

            var contract = _context.Contract.SingleOrDefault(m => m.Id == id);
            if (contract == null)
            {
                return NotFound();
            }

            return View(contract);
        }

        [HttpPost]
        public async Task<IActionResult> AddPhotoContract1(int? id, IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                var contract = _context.Contract.SingleOrDefault(m => m.Id == id);
                // путь к папке Files
                string path = "/images/Owner" + contract.OwnerId + "Contract1.jpg";
                if (path == contract.Photo1) path = "/images/Owner" + contract.OwnerId + "Contract1_1.jpg";
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }

                contract.Photo1 = path;
                _context.Update(contract);
                _context.SaveChanges();
                return RedirectToAction("PhotoContract", new { id = id, status = "Фото успешно загружено" });
            }

            return RedirectToAction("PhotoContract", new { id = id });
        }

        [HttpPost]
        public async Task<IActionResult> AddPhotoContract2(int? id, IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                var contract = _context.Contract.SingleOrDefault(m => m.Id == id);
                // путь к папке Files
                string path = "/images/Owner" + contract.OwnerId + "Contract2.jpg";
                if (path == contract.Photo2) path = "/images/Owner" + contract.OwnerId + "Contract2_1.jpg";
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }

                contract.Photo2 = path;
                _context.Update(contract);
                _context.SaveChanges();
                return RedirectToAction("PhotoContract", new { id = id, status = "Фото успешно загружено" });
            }

            return RedirectToAction("PhotoContract", new { id = id });
        }




        // GET: Owners/Create
        public IActionResult AddBox(int id)
        {
            OwnerRelView ww = new OwnerRelView { OwnerId = id };
            return View(ww);
        }

        // POST: Owners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddBox(OwnerRelView model)
        {
            if (ModelState.IsValid)
            {
                var box = _context.Boxes.SingleOrDefault(w => w.Number.Equals(model.BoxNumber));

                if (box != null)
                {

                    OwnerBoxRel ow = new OwnerBoxRel()
                    {
                        BoxId = box.Id,
                        OwnerId = model.OwnerId
                    };
                    _context.OwnerBoxRel.Add(ow);
                    await _context.SaveChangesAsync();
                    return RedirectToAction("Details", new { id = model.OwnerId, status = "Бокс добавлен" });
                }
                OwnerRelView ow1 = new OwnerRelView()
                {
                    OwnerId = model.OwnerId,
                    Status = "Бокс с таким номером не найден"
                };
                return View(ow1);
            };
            OwnerRelView ow2 = new OwnerRelView()
            {
                OwnerId = model.OwnerId,
            };
            return View(ow2);
        }


        public async Task<ActionResult> DeleteBox(int id)
        {
            OwnerBoxRel box = _context.OwnerBoxRel.FirstOrDefault(w => w.OwnerId == id);
            return View(box);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteConfirmBox(OwnerBoxRel cx)
        {
            //var box = _context.OwnerBoxRel.FirstOrDefault(m => m.BoxId == id);
            //if (box != null)
            {
                _context.Remove(cx);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Owners", new { id = cx.OwnerId, status = String.Format("Бокс успешно откреплен") });
            }
         //   return RedirectToAction("Details", "Owners", new { id = box.OwnerId, status = "Произошла ошибка" });

        }
        public ActionResult CancelDeleteBox(int id)
        {
            return RedirectToAction("Details", "Owners", new { id = id });
        }

    }
}
