﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GarageCooperative.Data;
using GarageCooperative.Models;
using Microsoft.AspNetCore.Authorization;
using GarageCooperative.Models.CreditDebitModels;
using GarageCooperative.Models.ArrivalJournalModels;

namespace GarageCooperative.Controllers
{
    [Authorize(Roles = "Администратор,Председатель,Охранник")]
    public class CreditDebitController : Controller
    {
        private readonly ApplicationDbContext _context;

        public CreditDebitController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Journals
        public async Task<IActionResult> Index(string status = null)
        {
            CreditDebitView view = new CreditDebitView
            {
                list = _context.CreditDebit.ToList(),
                Status = status,
                Balance = _context.balance.FirstOrDefault().balance
            };
            return View(view);
        }
        // GET: Credits/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var credit = await _context.CreditDebit
                .SingleOrDefaultAsync(m => m.Id == id);
            if (credit == null)
            {
                return NotFound();
            }

            return View(credit);
        }

        // GET: Credits/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Credits/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CreditDebit creditdebit)
        {
            
            if (ModelState.IsValid)
            {
                creditdebit.BuhAccepted = false;
                creditdebit.Data = DateTime.Now;
                _context.Add(creditdebit);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { status = "Запись успешно добавлена" });
            }
            return View(creditdebit);
        }

        // GET: Credits/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var credit = await _context.CreditDebit.SingleOrDefaultAsync(m => m.Id == id);
            if (credit == null)
            {
                return NotFound();
            }
            CreditDebitRecordView view = new CreditDebitRecordView
            {
                record = credit,
                Balance = _context.balance.FirstOrDefault().balance
            };


            return View(credit);
        }

        // POST: Credits/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(CreditDebitRecordView view)
        {

            if (ModelState.IsValid)
            
            {
                double Balance = _context.balance.FirstOrDefault().balance;
                if (view.record.BuhAccepted && view.record.Credit && view.record.Value>Balance)
                {
                    view.record.BuhAccepted = false;
                    view.Status = "Надостаточно средств на балансе";


                    CreditDebitRecordView view2 = new CreditDebitRecordView
                    {
                        record = view.record,
                        Balance = _context.balance.FirstOrDefault().balance
                    };
                    return View(view2);
                }
                if (view.record.BuhAccepted)
                {
                    view.record.ConfirmationData = DateTime.Now;
                    Balance balance = _context.balance.FirstOrDefault();
                    if (view.record.Credit) balance.balance -= view.record.Value;
                    else balance.balance += view.record.Value;
                    _context.Update(balance);
                    _context.Update(view.record);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            CreditDebitRecordView view1 = new CreditDebitRecordView
            {
                record = view.record,
                Balance = _context.balance.FirstOrDefault().balance
            };
            return View(view1);
        }
        // GET: Journals/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var journal = await _context.CreditDebit
                .SingleOrDefaultAsync(m => m.Id == id);
            if (journal == null)
            {
                return NotFound();
            }

            return View(journal);
        }

        // POST: Journals/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirm(int id)
        {
            var journal = await _context.CreditDebit.SingleOrDefaultAsync(m => m.Id == id);
            if (journal.BuhAccepted) return RedirectToAction("Index", new { status = "Нельзя удалить подтвержденную платежку" });
            _context.CreditDebit.Remove(journal);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { status = "Запись успешно удалена" });
        }


        public ActionResult Cancel() => RedirectToAction("Index");
    }
}
