﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using GarageCooperative.Models;
using GarageCooperative.Models.UsersViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace GarageCooperative.Controllers
{
    [Authorize(Roles = "Администратор")]
    public class UsersController : Controller
    {
        UserManager<ApplicationUser> _userManager;
        RoleManager<IdentityRole> _roleManager;

        public UsersController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        //public IActionResult Index() => View(_userManager.Users.ToList());
        public IActionResult Index(string status) {
            UserListViewModel model = new UserListViewModel()
            {
                Status = status,
                Userlist = _userManager.Users.ToList()
            };
           return View(model);
        }


        public IActionResult Create()
        {
            var allRoles = _roleManager.Roles.ToList();
            CreateUserViewModel model = new CreateUserViewModel()
            {
                AllRoles = allRoles,
                RoleId = allRoles.FirstOrDefault().Id
            };
            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> Create(CreateUserViewModel model, string submitBtn)
        {
            if (submitBtn.Equals("Отмена")) return RedirectToAction("Index");
            if (ModelState.IsValid)
            {
                ApplicationUser user = new ApplicationUser { Email = model.Email, UserName = model.UserName, OwnName = model.OwnName, ChangePassword = true };
                var result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, _roleManager.FindByIdAsync(model.RoleId).Result.Name);
                    return RedirectToAction("Index", new { status = System.String.Format("Пользователь {0} был успешно создан", user.UserName) });
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            model.AllRoles = new List<IdentityRole>(_roleManager.Roles.ToList());
            return View(model);
        }

        public async Task<IActionResult> Edit(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            // получем список ролей пользователя
            var userRoles = await _userManager.GetRolesAsync(user);
            var allRoles = _roleManager.Roles.ToList();
            var userRole = allRoles.Where(role => role.Name.Equals(userRoles.FirstOrDefault()));
            EditUserViewModel model = new EditUserViewModel()
            {
                Id = user.Id,
                Email = user.Email,
                OwnName = user.OwnName,
                UserRoles = userRoles,
                AllRoles = allRoles,
                RoleId = userRole.FirstOrDefault().Id,
                PasswordChange = user.ChangePassword
                
            };
            return View(model);
            //   EditUserViewModel model = new EditUserViewModel {Id = user.Id,  Email = user.Email, OwnName = user.OwnName };
            // return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(EditUserViewModel model, string submitBtn)
        {
            if (submitBtn.Equals("Отмена")) return RedirectToAction("Index");

            if (ModelState.IsValid)
            {
                ApplicationUser user = await _userManager.FindByIdAsync(model.Id);
                if (submitBtn.Equals("Сменить пароль"))
                {
                    user.ChangePassword = true;
                    var result = await _userManager.UpdateAsync(user);
                    return RedirectToAction("Index", new { status = System.String.Format("Пароль пользователя {0} был успешно сброшен", user.UserName) });
                }
                if (user != null)
                {
                    user.Id = model.Id;
                    user.Email = model.Email;
                    //user.UserName = model.UserName;
                    user.OwnName = model.OwnName;
                    user.ChangePassword = model.PasswordChange;

                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {

                        foreach (IdentityRole role in _roleManager.Roles.ToList())
                        {
                            await _userManager.RemoveFromRoleAsync(user, role.Name);
                        }
                        await _userManager.AddToRoleAsync(user, _roleManager.FindByIdAsync(model.RoleId).Result.Name);
                        return RedirectToAction("Index", new { status = System.String.Format("Пользователь {0} был успешно изменен", user.UserName) });
                    }
                   /* else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }

                        model.AllRoles = new List<IdentityRole>(_roleManager.Roles.ToList());

                        return View(model);
                    }
                    */
                }
            };
            /* foreach (ModelError error in ModelState.Values.SelectMany (v => v.Errors))
             {
                 ModelState.AddModelError(string.Empty, error.ErrorMessage);
             }*/
           // ModelState.AddModelError("myerrorsummary", "The input is not valid");
            model.AllRoles = new List<IdentityRole>(_roleManager.Roles.ToList());
            return View(model);
        }
        public async Task<ActionResult> Delete(string id)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(id);
            DeleteUserViewModel model = new DeleteUserViewModel { Id = user.Id, UserName = user.UserName };
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(DeleteUserViewModel model)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(model.Id);
            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
            }
            return RedirectToAction("Index", new { status = System.String.Format("Пользователь {0} был успешно удален", user.UserName) });
        }
        public ActionResult Cancel(DeleteUserViewModel model) => RedirectToAction("Index");
    }
}
