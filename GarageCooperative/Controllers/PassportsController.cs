﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using GarageCooperative.Data;
using GarageCooperative.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GarageCooperative.Controllers
{
    [Authorize(Roles = "Администратор,Председатель")]
    public class PassportsController : Controller
    {
        private readonly ApplicationDbContext _context;
        IHostingEnvironment _appEnvironment;

        public PassportsController(ApplicationDbContext context, IHostingEnvironment appEnvironment)
        {
            _context = context;
            _appEnvironment = appEnvironment;
        }

        // GET: Owners/Details/5
        public async Task<IActionResult> Edit(int? id, string status = null)
        {
            if (id == null)
            {
                return NotFound();
            }

            var passport = _context.Passports.SingleOrDefault(m => m.Id == id);
            if (passport == null)
            {
                return NotFound();
            }

            return View(passport);
        }

        // GET: Owners/Details/5
        public async Task<IActionResult> Photo(int? id, string status = null)
        {
            if (id == null)
            {
                return NotFound();
            }

            var passport = _context.Passports.SingleOrDefault(m => m.Id == id);
            if (passport == null)
            {
                return NotFound();
            }

            return View(passport);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("Id,Seria,Number,DateOfBirth,PlaceOfBirth,DateOfIssue,PlaceOfIssue,OwnerId,Photo1,Photo2")] Passport passport)
        {
            if (ModelState.IsValid)
            {
                _context.Update(passport);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Owners", new { id = passport.OwnerId, status = "Паспортные данные успешно изменены" });
            }
            return View(passport);
        }

        [HttpPost]
        public async Task<IActionResult> AddPhoto1(int? id, IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                var passport = _context.Passports.SingleOrDefault(m => m.Id == id);
                // путь к папке Files
                string path = "/images/Owner" + passport.OwnerId +"Passport1.jpg";
                if (path == passport.Photo1) path = "/images/Owner" + passport.OwnerId + "Passport1_1.jpg";
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }

                passport.Photo1 = path;
                _context.Update(passport);
                _context.SaveChanges();
                return RedirectToAction("Photo", new { id = id, status = "Фото успешно загружено" });
            }

            return RedirectToAction("Photo", new { id = id, status = "Фото не загружено" });
        }

        [HttpPost]
        public async Task<IActionResult> AddPhoto2(int? id, IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                var passport = _context.Passports.SingleOrDefault(m => m.Id == id);
                // путь к папке Files
                string path = "/images/Owner" + passport.OwnerId + "Passport2.jpg";
                if (path == passport.Photo1) path = "/images/Owner" + passport.OwnerId + "Passport2_1.jpg";
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }

                passport.Photo2 = path;
                _context.Update(passport);
                _context.SaveChanges();
                return RedirectToAction("Photo", new { id = id, status = "Фото успешно загружено" });
            }

            return RedirectToAction("Photo", new { id = id, status = "Фото не загружено" });
        }


        // GET: Owners/Create
        public IActionResult Create(int id)
        {
            Passport passport = new Passport();
            passport.OwnerId = id;
            return View(passport);
        }

        // POST: Owners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Passport passport)
        {
            if (ModelState.IsValid)
            {
                //var owner = _context.Owners.SingleOrDefault(w => w.Id == passport.OwnerId);
                //owner.Passport.Add(passport);
                // owner.Contract.Number = contractNumber;
                passport.Id = 0;
                _context.Add(passport);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Owners", new { id=passport.OwnerId, status = String.Format("Паспорт добавлен успешно") });
            }
            return View(passport);
        }

        public async Task<ActionResult> Delete(int id)
        {
            Passport passport = _context.Passports.SingleOrDefault(w => w.Id == id);
            return View(passport);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteConfirm(int id)
        {
            var passport = _context.Passports.SingleOrDefault(m => m.Id == id);
            if (passport != null)
            {

                _context.Remove(passport);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Owners", new { id = passport.OwnerId, status = String.Format("Паспорт успешно удален") });
            }
            return RedirectToAction("Details", "Owners", new { id = passport.OwnerId, status = "Произошла ошибка" });

        }
        public ActionResult Cancel(int id)
        {
            return RedirectToAction("Details", "Owners", new { id = id });
        }

    }
}