﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GarageCooperative.Data;
using GarageCooperative.Models;
using Microsoft.AspNetCore.Authorization;
using GarageCooperative.Models.ArrivalJournalModels;

namespace GarageCooperative.Controllers
{
    [Authorize(Roles = "Администратор,Председатель,Охранник")]
    public class EventJournalsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EventJournalsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Journals
        public async Task<IActionResult> Index(string status=null)
        {
            EventJournalView view = new EventJournalView
            {
                journal = _context.EventJournal.ToList(),
                Status=status
            };
            return View(view);
        }

        // GET: Journals/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var journal = await _context.EventJournal
                .SingleOrDefaultAsync(m => m.Id == id);
            if (journal == null)
            {
                return NotFound();
            }

            return View(journal);
        }
        
        // GET: Journals/Create
        public IActionResult Create(string status = null)
        {
            EventJournalRecord record = new EventJournalRecord();
            EventJournalRecordView view = new EventJournalRecordView
            {
                Status = status
            };
            return View(view);
        }

        // POST: Journals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EventJournalRecordView view, string Decription)
        {
            EventJournalRecord record = new EventJournalRecord();

            if ( Decription!=null)
            {
                record.Data = DateTime.Now;
                record.Decription = Decription;
                    _context.Add(record);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { status = "Запись успешно добавлена" });
            }

            return RedirectToAction("Create", new { status = "Запрещено добавлять пустые записи" });
        }

        // GET: Journals/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var journal = await _context.EventJournal
                .SingleOrDefaultAsync(m => m.Id == id);
            if (journal == null)
            {
                return NotFound();
            }

            return View(journal);
        }

        // POST: Journals/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirm(int id)
        {
            var journal = await _context.EventJournal.SingleOrDefaultAsync(m => m.Id == id);
            _context.EventJournal.Remove(journal);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { status = "Запись успешно удалена" });
        }

        private bool JournalExists(int id)
        {
            return _context.ArrivalJournal.Any(e => e.Id == id);
        }

        public ActionResult Cancel() => RedirectToAction("Index");
    }
}
