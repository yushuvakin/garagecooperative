﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using GarageCooperative.Data;
using GarageCooperative.Models;
using Microsoft.AspNetCore.Authorization;
using GarageCooperative.Models.ArrivalJournalModels;

namespace GarageCooperative.Controllers
{
    [Authorize(Roles = "Администратор,Председатель,Охранник")]
    public class ArrivalJournalsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ArrivalJournalsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Journals
        public async Task<IActionResult> Index(string status=null)
        {
            ArrivalJournalView view = new ArrivalJournalView
            {
                journal = _context.ArrivalJournal.Include(x => x.Box).ToList(),
                Status=status
            };
            return View(view);
        }

      /*  // GET: Journals/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var journal = await _context.ArrivalJournal
                .SingleOrDefaultAsync(m => m.Id == id);
            if (journal == null)
            {
                return NotFound();
            }

            return View(journal);
        }
        */
        // GET: Journals/Create
        public IActionResult Create(string status = null)
        {
            ArrivalJournalRecord record = new ArrivalJournalRecord();
            ArrivalJournalRecordView view = new ArrivalJournalRecordView
            {
                record = record,
                Status = status
            };
            return View(view);
        }

        // POST: Journals/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ArrivalJournalRecordView view, string Purpose)
        {
            ArrivalJournalRecord record;
            Box box = _context.Boxes.SingleOrDefault(w => w.Number.Equals(view.BoxNumber));

            if (ModelState.IsValid)
            {
                if (box == null && view.BoxNumber!=null)
                {
                    record = new ArrivalJournalRecord
                    {
                        Name = view.record.Name,
                        CarNumber = view.record.CarNumber,
                        Purpose = Purpose
                    };
                    ArrivalJournalRecordView view1 = new ArrivalJournalRecordView
                    {
                        record = record,
                        Status = "Бокса с данным номером не существует",
                    };
                    return View(view1);
                }
                view.record.Data = DateTime.Now;
                if (view.BoxNumber != null) view.record.BoxId = box.Id;
                else view.record.Box = null;
                view.record.Purpose = Purpose;
                    _context.Add(view.record);
                await _context.SaveChangesAsync();
                return RedirectToAction("Index", new { status = "Запись успешно добавлена" });
            }
            record = new ArrivalJournalRecord
            {
                Name = view.record.Name,
                CarNumber = view.record.CarNumber,
                Purpose = view.record.Purpose
            };
            ArrivalJournalRecordView view2 = new ArrivalJournalRecordView
            {
                record = record,
                BoxNumber = view.BoxNumber
            };
            return View(view2);
        }

        // GET: Journals/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var journal = await _context.ArrivalJournal
                .SingleOrDefaultAsync(m => m.Id == id);
            if (journal == null)
            {
                return NotFound();
            }

            return View(journal);
        }

        // POST: Journals/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirm(int id)
        {
            var journal = await _context.ArrivalJournal.SingleOrDefaultAsync(m => m.Id == id);
            _context.ArrivalJournal.Remove(journal);
            await _context.SaveChangesAsync();
            return RedirectToAction("Index", new { status = "Запись успешно удалена" });
        }

        private bool JournalExists(int id)
        {
            return _context.ArrivalJournal.Any(e => e.Id == id);
        }

        public ActionResult Cancel() => RedirectToAction("Index");
    }
}
