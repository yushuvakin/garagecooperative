﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using GarageCooperative.Data;
using GarageCooperative.Models;
using GarageCooperative.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using System.IO;
using Microsoft.Extensions.FileProviders;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;

namespace GarageCooperative
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
            .AddErrorDescriber<RussianIdentityErrorDescriber>()
            .AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders()
           ;
            
            // 10 minutes wait if 10 failed login attempts
            services.Configure<IdentityOptions>(options =>
            {
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);
                options.Lockout.MaxFailedAccessAttempts = 5;
            });

            // Add application services.
            services.AddTransient<IEmailSender, EmailSender>();
            services.AddMvc(config =>
            {
                var policy = new AuthorizationPolicyBuilder()
                                 .RequireAuthenticatedUser()
                                 .Build();
                config.Filters.Add(new AuthorizeFilter(policy));
            });
            services.AddMvc().AddRazorPagesOptions(options =>
            {
                //options.Conventions.AuthorizePage("/Owners");
                //options.Conventions.AuthorizePage("/Logs");
                //options.Conventions.AuthorizePage("/Journals");
                //options.Conventions.AuthorizePage("/Credits");
                //options.Conventions.AuthorizePage("/Debits");
                options.Conventions.AllowAnonymousToFolder("/Home");

            });
           

            services.AddAuthorization(options =>
            {
                options.AddPolicy("RequireAdministratorRole", policy => policy.RequireRole("Administrator"));
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseIdentity();
            app.UseMvcWithDefaultRoute();
            //app.UseMvc(routes =>
            //{
            //    routes.MapRoute(
            //        name: "default",
            //        template: "{controller=Home}/{action=Index}/{id?}");
            //});
           // try
          //  {
            //    SeedData.Initialize(app.ApplicationServices, "Qwerty1234_").Wait();
           // }
            //catch
            //{
            //    throw new System.Exception("You need to update the DB "
            //        + "\nPM > Update-Database " + "\n or \n" +
            //          "> dotnet ef database update"
            //          + "\nIf that doesn't work, comment out SeedData and register a new user");
            //}

        }
    }
}
